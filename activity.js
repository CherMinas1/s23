
db.hotels.insertOne({
        name: "Single",
        accomodates: 2,
        price: 1000,
        description: "A simple room with all the basic necessities.",
        rooms_avialable: 10,
        isAvailable: false
    
    }
)

db.hotels.insertMany([
    {
        name: "Double",
        accomodates: 3,
        price: 2000,
        description: "A room fit for a small family going on a vacation.",
        rooms_avialable: 5,
        isAvailable: false
        },
    {
        name: "Queen",
        accomodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway.",
        rooms_avialable: 15,
        isAvailable: false
        } 
    
    ]
    
)

db.hotels.find(
   { name: "Double"
       } 
    
    )
//6

db.hotels.updateOne(
    {rooms_avialable: 15 },
    {$set: {
        name: "Queen",
        accomodates: 4,
        price: 4000,
        description: "A room with a queen sized bed perfect for a simple getaway.",
        rooms_avialable: 0,
        isAvailable: false
        }   
    
  }
)

//7

db.hotels.deleteMany({
	rooms_avialable: 0 
})
